import json

from flask import request
from run import app

from .services import StatusInfoApiService, StatusInfoMqttService
from .schemas import StatusInfoSchema, StatusInfoUpdateSchema


@app.route('/api/sensors/', methods=['GET'])
def get_status_info_list():
    status_info_service = StatusInfoApiService()
    status_info_list = status_info_service.get_status_list()
    return {'statuses':
            StatusInfoSchema().dump(status_info_list, many=True), },\
        200


@app.route('/api/sensors/<int:id>', methods=['GET'])
def get_status_info(id):
    status_info_service = StatusInfoApiService()
    status_info = status_info_service.get(id)
    return StatusInfoSchema().dumps(status_info), 200


@app.route('/api/sensors/<int:id>', methods=['DELETE'])
def delete_status_info(id):
    status_info_service = StatusInfoApiService()
    success = status_info_service.delete(id)
    if success == 1:
        return json.dumps({"success": True, "id": id}), 200
    else:
        return json.dumps({"success": False, "id": id}), 200


@app.route('/api/sensors/<int:id>', methods=['POST'])
def update_status_info(id):
    status_info_service = StatusInfoApiService()
    status_info_mqtt_service = StatusInfoMqttService()
    status_info = StatusInfoUpdateSchema().load(request.json)
    sensor_guid = status_info_service.update(id, status_info)
    if sensor_guid is not False:
        status_info_mqtt_service.set_sensor_status_info(status_info['status'],
                                                        sensor_guid)
        return json.dumps({"success": True, "id": id}), 200
    else:
        return json.dumps({"success": False, "id": id}), 200
