from marshmallow import Schema, fields, post_load


class StatusInfoSchema(Schema):
    id = fields.Integer()
    status = fields.Integer()
    guid = fields.Str()
    register_date = fields.DateTime()


class StatusInfoUpdateSchema(Schema):
    status = fields.Integer()
    @post_load
    def make_status_info(self, data, **kwargs):
        return data


class StatusHistorySchema(Schema):
    id = fields.Integer()
    id_status_info = fields.Integer()
    start = fields.DateTime()
    stop = fields.DateTime()
    active = fields.Boolean()
