from datetime import datetime
from run import db, mqtt
from .schemas import StatusInfoUpdateSchema
from .models import StatusInfo


class StatusInfoApiService(object):
    def get_status_list(self):
        status_info_list = StatusInfo.query.all()
        return status_info_list

    def get(self, id):
        status_info = StatusInfo.query.filter_by(id=id).first()
        return status_info

    def delete(self, id):
        status_info = StatusInfo.query.filter_by(id=id).first()
        if status_info is not None:
            db.session.delete(status_info)
            db.session.commit()
            return 1
        else:
            return 0

    def update(self, sensor_id, status_info):
        current_status_info = StatusInfo.query.filter_by(
            id=sensor_id).first()
        if current_status_info is not None:
            current_status_info.status = status_info['status']
            db.session.commit()
            return current_status_info.guid
        else:
            return False

    def create(self, create_request, guid):
        duplicate_status_info = StatusInfo.query.filter_by(
            guid=guid).first()
        if duplicate_status_info is not None:
            return duplicate_status_info
        else:
            register_date = datetime.now()
            new_status_info = StatusInfo(
                register_date=register_date,
                guid=guid)
            db.session.add(new_status_info)
            db.session.commit()
            return new_status_info


class StatusInfoMqttService(object):
    def update_status(self, status, sensor_guid):
        current_status_info = StatusInfo.query.filter_by(
            guid=sensor_guid).first()
        if current_status_info is not None:
            current_status_info.status = status['status']
            db.session.commit()
        else:
            self.create(status, sensor_guid)

    def set_sensor_status_info(self, status, sensor_guid):
        topic = "sensors/lighting/%s/command" % sensor_guid
        mqtt.publish(topic,
                     StatusInfoUpdateSchema().dumps({'status': status}),
                     qos=2)

    def create(self, status, sensor_guid):
        register_date = datetime.now()
        new_status_info = StatusInfo(
            status=status['status'],
            register_date=register_date,
            guid=sensor_guid)
        db.session.add(new_status_info)
        db.session.commit()
