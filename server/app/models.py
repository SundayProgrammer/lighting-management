from run import db


class StatusInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    status = db.Column(db.Integer)
    register_date = db.Column(db.DateTime)
    guid = db.Column(db.String(128))
    status_info = db.relationship('StatusHistory',
                                  backref=db.backref('status_info', lazy=True))

    def __init__(self, **kwargs):
        super(StatusInfo, self).__init__(**kwargs)

    def __repr__(self):
        return '<Status {}>'.format(self.status)


class StatusHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    id_status_info = db.Column(db.Integer, db.ForeignKey('status_info.id'))
    start = db.Column(db.DateTime)
    stop = db.Column(db.DateTime)
    active = db.Column(db.Boolean)
    
    def __init__(self, **kwargs):
        super(StatusHistory, self).__init__(**kwargs)

    def __repr__(self):
        return '<Last Status {}>'.format(self.active)
