from run import mqtt
from .schemas import StatusInfoUpdateSchema
from .services import StatusInfoMqttService, StatusInfoApiService


@mqtt.on_topic("sensors/lighting/+/status")
def handle_sensor_status_info(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    status_info_mqtt_service = StatusInfoMqttService()
    print(data['payload'])
    status = StatusInfoUpdateSchema().loads(data['payload'])
    sensor_guid = data['topic'].split("/")[2]
    status_info_mqtt_service.update_status(status, sensor_guid)


# @mqtt.on_topic("sensors/lighting/+/will")
# def handle_sensor_last_will(client, userdata, message):
#     data = dict(
#         topic=message.topic,
#         payload=message.payload.decode()
#     )
