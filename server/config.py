
import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # database setup
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + \
        os.path.join(basedir, 'lighting.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # mqtt connection setup
    MQTT_BROKER_URL = 'localhost'
    MQTT_BROKER_PORT = 1883
    MQTT_USERNAME = ''
    MQTT_PASSWORD = ''
    MQTT_KEEPALIVE = 500
    MQTT_TLS_ENABLED = False
