from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mqtt import Mqtt

from config import Config


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    return app


app = create_app()
db = SQLAlchemy(app)
migrate = Migrate(app, db)
mqtt = Mqtt(app)


mqtt.subscribe("sensors/lighting/+/status", qos=1)
mqtt.subscribe("sensors/lighting/+/will", qos=1)

from app import models, routes, mqtt_handler

if __name__ == '__main__':
    app.run(debug=False, host='127.0.0.1', use_reloader=False)
