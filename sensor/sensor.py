import argparse
import hashlib
import json
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import threading

from schemas import StatusInfoUpdateSchema

available = threading.RLock(0)
STATUS = None


class MqttCommunicationService(mqtt.Client):
    def __init__(self, guid, topics, topics_callbacks, **kwargs):
        super(MqttCommunicationService, self).__init__(**kwargs)
        self.guid = guid
        self.topics = topics
        self.topics_callbacks = topics_callbacks

    def on_message(self, mqttc, obj, msg):
        command_topic_name = "sensors/lighting/%s/command" % self.guid
        if command_topic_name == msg.topic:
            try:
                self.topics_callbacks[command_topic_name](msg.topic,
                                                          msg.payload)
            except KeyError:
                print("No handle for this topic!")
            except Exception as e:
                print(e)
        else:  # nobreak
            print("Device doesn't process [%s] topic!" % msg.topic)

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("\nSubscribed to " + str(mid) +
              " with qos = " + str(granted_qos))

    def start(self, topics_to_subscribe, host, status_topic):
        self.connect(host, port=1883, keepalive=300)
        for topic in self.topics:
            self.subscribe(topic['name'], topic['qos'])

        publish.single(status_topic,
                       payload=json.dumps({'status': 0}),
                       hostname=host,
                       keepalive=200,
                       qos=1)
        self.loop_start()


class DeviceMock(object):
    def __init__(self,
                 device_guid,
                 host="localhost"):
        self.guid = device_guid
        self.host = "localhost"
        self.last_status = None

    def console_interface(self, current_state):
        """
        Method allows user to check current state of sensor and
        also to set it in on or off state.
        """
        print("Sensor state: %s" % ("OFF" if current_state == 0 else "ON"))
        print("(0/1 - state setup options; Ctrl + c - exit)")
        user_input = input(">>>>")
        return self.process_input(user_input, current_state)

    def process_input(self, user_input, current_state):
        """
        Method performs action based on user provided input

        :param user_input: user input from console
        """
        mqtt_message = None
        try:
            if int(user_input) == 0 and current_state != 0:
                mqtt_message = json.dumps({"status": 0})
                self.last_status = 0
            elif int(user_input) == 1 and current_state != 1:
                mqtt_message = json.dumps({"status": 1})
                self.last_status = 1
            elif self.last_status != current_state:
                mqtt_message = json.dumps({"status": current_state})
                self.last_status = current_state
        except ValueError:
            pass
        else:
            mqtt_topic = "sensors/lighting/%s/status" % self.guid
            if mqtt_message is not None:
                publish.single(mqtt_topic,
                               payload=mqtt_message,
                               hostname=self.host,
                               keepalive=200,
                               qos=1)
        finally:
            return self.last_status


def process_command_message(topic, payload):
    global STATUS, available
    try:
        command = StatusInfoUpdateSchema().loads(payload)['status']
    except Exception as e:
        print(e)
    else:
        available.acquire()
        STATUS = int(command)
        available.release()


def console_app_loop(mock_device):
    """
    Function loops over user input function and eventualy saves new state
    to global `STATE` variable
    """
    global STATUS, available
    while True:
        rc = mock_device.console_interface(STATUS)
        if rc is not None:
            available.acquire(blocking=False)
            STATUS = rc
            available.release()


def get_params():
    parser = argparse.ArgumentParser()
    parser.add_argument("seed", help="unique value to seed md5 algorithm")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    params = vars(get_params())
    device_guid = hashlib.md5(str(params['seed']).encode('utf-8')).hexdigest()

    topics_callbacks = {("sensors/lighting/%s/command" %
                         device_guid): process_command_message, }
    topics_to_subscribe = [
        dict(name=("sensors/lighting/%s/command" % device_guid), qos=1), ]

    mqtt_communication_service = MqttCommunicationService(device_guid,
                                                          topics_to_subscribe,
                                                          topics_callbacks)
    mock_device = DeviceMock(device_guid, "localhost")

    print("Wellcome in MOCK DEVICE control panel")
    print("Device ID: %s" % device_guid)

    mqtt_communication_service.start(
        topics_to_subscribe, "localhost",
        "sensors/lighting/%s/status" % device_guid
    )

    console_app_thread = threading.Thread(target=console_app_loop(mock_device))
    console_app_thread.start()
    console_app_loop.join()
