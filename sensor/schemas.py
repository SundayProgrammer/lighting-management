from marshmallow import Schema, fields, post_load


class StatusInfoUpdateSchema(Schema):
    status = fields.Integer()
    @post_load
    def make_status_info(self, data, **kwargs):
        return data
