from marshmallow import Schema, fields, post_load


class StatusUpdateResponseSchema(Schema):
    success = fields.Boolean()
    id = fields.Integer()


class StatusInfoResponseSchema(Schema):
    id = fields.Integer()
    status = fields.Integer()
    guid = fields.Str()
    register_date = fields.DateTime()