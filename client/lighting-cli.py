import json
import argparse
import requests

from schemas import StatusInfoResponseSchema, StatusUpdateResponseSchema


class RestAPIService(object):
    def get_points_list(self):
        try:
            r = requests.get('http://127.0.0.1:5000/api/sensors/')
            data = r.json()
            id_list, status_list = [], []
            for point in data['statuses']:
                id_list.append(point['id'])
                status_list.append(point['status'])
        except Exception as e:
            print("Request failure")
        else:
            for point in zip(id_list, status_list):
                print("Sensor: id=%d, status=%s" % (
                    point[0], "ON" if point[0] == 1 else "OFF"))

    def get_point(self, point_id):
        try:
            r = requests.get('http://127.0.0.1:5000/api/sensors/%s' % point_id)
            data = r.json()
            id = data['id']
            status = data['status']
            guid = data['guid']
            register_date = data['register_date']
        except Exception as e:
            print("Request failure")
        else:
            print("Sensor %d details" % id)
            print("Status: %s" % "ON" if status == 1 else "OFF")
            print("Guid: %s" % guid)
            print("Register date: %s" % register_date)

    def set_point(self, point_id, status_value):
        try:
            payload = {"status": status_value}
            headers = {"Content-Type": "application/json"}
            r = requests.post('http://127.0.0.1:5000/api/sensors/%s' % (
                point_id), data=json.dumps(payload), headers=headers)
            data = r.json()
        except Exception as e:
            print("Request failure")
        else:
            if data['success']:
                print("Sensor id=%d value changed to %s" % (data['id'],
                                                            status_value))
            else:
                print("Sensor id=%d value update failure")


def get_params():
    parser = argparse.ArgumentParser(
        description="CLI for lighting sensors C&C server.")
    root_group = parser.add_mutually_exclusive_group()
    group1 = root_group.add_mutually_exclusive_group()
    group2 = root_group.add_argument_group()
    group1.add_argument("-gl", "--get_list", help="fetch list of lighting points",
                        action="store_true")
    group1.add_argument("-gp", "--get_point", help="fetch full information about chosen lighting point",
                        type=int)
    group2.add_argument("-sp", "--set_point", help="sets lighting point in ON/OFF state",
                        type=int)
    group2.add_argument("-v", "--value", help="1/0 for config of ON/OFF state",
                        type=int)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    params = vars(get_params())
    rest_api_service = RestAPIService()
    if params['get_list']:
        rest_api_service.get_points_list()
    elif params['get_point']:
        rest_api_service.get_point(params['get_point'])
    elif params['set_point'] is not None and params['value'] is not None:
        rest_api_service.set_point(params['set_point'], params['value'])
    else:
        print("Chose other params")
