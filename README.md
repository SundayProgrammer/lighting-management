# Lighting management

Simple application with REST API and MQTT client for building lighting management.
(Tested on Python 3.8)

## Running the app

### Virtual Environment
It is the best to have some virtual environment created for the project.

It can be created i.e. this way:
```
virtualenv lighting --python=[path to chosen python version]
# activate virtualenv
.\lighting\Scripts\activate.bat  # Windows
source ./lighting/bin/activate   # Linux
```
When starting new console for other stages, activate also virtualenv in every terminal instance.

### Mosquitto Broker
Installation instructions nad downloads can be found on this [page](https://mosquitto.org/download/).

### API server

```
cd server
pip install -r requirements.txt

set FLASK_APP=run.py  # Windows
set FLASK_DEBUG=0  # Windows
export FLASK_APP=run.py  # Linux
export FLASK_DEBUG=0  # Linux
```

### Mock Device

```
cd sensor
pip install -r requirements.txt

python sensor.py [seed - unique for different devices]
```

### API Client

```
cd sensor
pip install -r requirements.txt

python client.py  [-h] [[-gl | -gp GET_POINT]] [-sp SET_POINT] [-v VALUE]
```


## REST API

### Endpoint: **/api/sensors**
GET:
```json
{
    "statuses": [
        {...},
        {...}
    ]
}
```
### Endpoint: **/api/sensors/{id}**
GET:
```json
{
    "id": 1,
    "status": 1,
    "guid": "5a105e8b9d40e1329780d62ea2265d8a",
    "register_date": "2020-02-01T09:15:36.259649"
}
or
{}
```
DELETE (success):
```json
{
    "success": true,
    "id": 20
}
```

DELETE (failure):
```json
{
    "success": false,
    "id": 20
}
```

POST (success - return updated object):
```json
request:
{
    "status": 1
}
response:
{
    "success": true,
    "id": 20
}
```
POST (failure):
```json
{
    "success": false,
    "id": 20
}
```

## MQTT communicates

### Topic: **sensors/lighting/[sensor id]/status**
Sensor to Mocquitto Broker Status Communicate
```json
{
    "status": [0/1]
}
```

### Topic: **sensors/lighting/[sensor id]/command**
Command Server to Mocquitto Broker Command Communicate
```json
{
    "status": [0/1]
}
```

### Topic: **sensors/lighting/[sensor id]/will**
Sensor to Mocquitto Broker Last Will Communicate
```json
{
    "status": 2
}
```